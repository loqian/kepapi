from django.shortcuts import render
from django.http import HttpResponse
from utils.keeplived import Keepalived
import os

# Create your views here.
def get_status(request):
        k = Keepalived('192.168.16.88')
        status = k.check_keepalived_status
        return HttpResponse(status)
        # 返回list列表，传递给home.html模版
        # return render(request, s)
        # return render(request, 'home.html')

