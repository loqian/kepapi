from django.apps import AppConfig


class KeealivedApiConfig(AppConfig):
    name = 'keealived_api'
