import os
import time
import re


class Keepalived(object):
    def __init__(self, vip):
        self.vip = vip


    def modify_config(self, state, priority):
        os.system('sed -i "s/priority .*/priority {}/g" /etc/keepalived/keepalived.conf;'
                  .format(priority))
        os.system('sed -i "s/state .*/state {}/g" /etc/keepalived/keepalived.conf;'
                  .format(state))
        os.system('kill -HUP $(cat /var/run/keepalived.pid)')



    def add_config(self):
        pass



    def check_process(self):
        check_peroid = 1
        while check_peroid > 0:
            #mysql,dhcp
            server_port = ['9000', '3000', '3341']
            for port in server_port:
                outp = os.popen('netstat -tunlp | grep {}'.format(port)).read().strip()
                if outp == 0:
                    check_peroid -= 1
                else:
                    print('=====checking====')
                    time.sleep(3)
        exe = os.system('sed -i "s/state .*/state BACKUP/g" /etc/keepalived/keepalived.conf;'
                        'sed -i "s/priority .*/priority 0/g" /etc/keepalived/keepalived.conf')
        if exe == 0:
            os.system('systemctl restart keepalived')
            print('====Keepalived config will be changed at {}============='.format(time.strftime("%Y-%m-%d %H:%M:%S", time.localtime())))
            string = os.popen('curl -s {}'.format(self.vip)).read().strip()
            if 'HA2' or 'HA3' in string:
               print(string)
            else:
               print('Server-Error')
        else:
            exe = os.system('killall keepalived')
            if exe == 0:
               print('keepalived be killed!')


    def check_keepalived_status(self):
        states = ['Master', 'Slave']
        outp = os.popen('id addr show').read().strip()
        state = re.findall(r'{}'.format(self.vip), outp)
        if self.vip in state:
            return states[0]
        else:
            return states[1]


if __name__ == "__main__":
  pass

